## Taxi Service
There are two microservices. Taxi service using map service to calculate the distance between given points and calculate taxi fare with retrieved distance value.
Both microservices are providing REST API. There is also eureka naming server for pairing taxi service instances and map service instances. Both services are registering 
eureka and eureka providing the corresponding addresses. Taxi service is also using Ribbon for load balancing. 

## Motivation
This is an exercise project to go into learning spring boot, microservices and IPC. 
Project is using Spring Boot, Maven, Feign, Ribbon and Eureka.

## Installation
You can download the project using git
```
git clone https://mucahitbaydar@bitbucket.org/mucahitbaydar/taxi-service.git
```

Then you can build each service and create jar file using maven

```
mvn install
```

##Usage
First you can start Eurekanamingserver then you can start other two microservices.
Once everything started succesfully
(Assuming you didn't change any configuration)

You can send a request with 
```
http://localhost:8100/taxi-service-feign/from/42.269,12.269/to/42.111,12.111
```
