package com.baydar.taxiservice.model;

public class TaxiFare {
	
	private double lat1;
	private double lon1;
	private double lat2;
	private double lon2;
	
	private double distance;
	private double fare;
	
	private int port;
	
	public TaxiFare() {
		
	}
	
	public TaxiFare(double lat1, double lon1, double lat2, double lon2, double distance, double fare, int port) {
		this.lat1 = lat1;
		this.lon1 = lon1;
		this.lat2 = lat2;
		this.lon2 = lon2;
		this.distance = distance;
		this.fare = fare;
		this.port = port;
	}
	
	public double getFare() {
		return fare;
	}
	
	public double getLat1() {
		return lat1;
	}
	
	public double getLon1() {
		return lon1;
	}
	
	public double getLat2() {
		return lat2;
	}
	
	public double getLon2() {
		return lon2;
	}
	
	public void setPort(int port) {
		this.port = port;
	}
	
	public int getPort() {
		return this.port;
	}
	
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	public double getDistance() {
		return this.distance;
	}


}
