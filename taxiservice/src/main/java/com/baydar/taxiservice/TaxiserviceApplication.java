package com.baydar.taxiservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients("com.baydar.taxiservice")
@EnableDiscoveryClient
public class TaxiserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaxiserviceApplication.class, args);
	}

}
