package com.baydar.taxiservice.proxy;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.baydar.taxiservice.model.TaxiFare;

@FeignClient(name = "map-service")
@RibbonClient(name = "map-service")
public interface MapServiceProxy {
	@GetMapping("/map-service/from/{lat1},{lon1}/to/{lat2},{lon2}")
	public TaxiFare retrieveDistanceValue(@PathVariable("lat1") Double lat1, @PathVariable("lon1") Double lon1,
			@PathVariable("lat2") Double lat2, @PathVariable("lon2") Double lon2);
}
