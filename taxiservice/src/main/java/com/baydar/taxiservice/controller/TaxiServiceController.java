package com.baydar.taxiservice.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.baydar.taxiservice.model.TaxiFare;
import com.baydar.taxiservice.proxy.MapServiceProxy;

@RestController
public class TaxiServiceController {
	
	@Autowired
	private MapServiceProxy proxy;

	@GetMapping("/taxi-service/from/{lat1},{lon1}/to/{lat2},{lon2}")
	public TaxiFare calculateFare(@PathVariable Double lat1, @PathVariable Double lon1, @PathVariable Double lat2,
			@PathVariable Double lon2) {

		Map<String, Double> uriVariables = new HashMap<>();
		uriVariables.put("lat1", lat1);
		uriVariables.put("lon1", lon1);
		uriVariables.put("lat2", lat2);
		uriVariables.put("lon2", lon2);

		ResponseEntity<TaxiFare> responseEntity = new RestTemplate().getForEntity(
				"http://localhost:8000/map-service/from/{lat1},{lon1}/to/{lat2},{lon2}", TaxiFare.class, uriVariables);

		TaxiFare response = responseEntity.getBody();

		return new TaxiFare(lat1, lon1, lat2, lon2, response.getDistance(), response.getDistance() * 5,
				response.getPort());
	}
	
	@GetMapping("/taxi-service-feign/from/{lat1},{lon1}/to/{lat2},{lon2}")
	public TaxiFare calculateFareFeign(@PathVariable Double lat1, @PathVariable Double lon1, @PathVariable Double lat2,
			@PathVariable Double lon2) {

		TaxiFare response = proxy.retrieveDistanceValue(lat1,lon1,lat2,lon2);

		return new TaxiFare(lat1, lon1, lat2, lon2, response.getDistance(), response.getDistance() * 5,
				response.getPort());
	}

}
