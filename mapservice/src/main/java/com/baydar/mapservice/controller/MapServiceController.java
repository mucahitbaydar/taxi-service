package com.baydar.mapservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.baydar.mapservice.model.DistanceValue;
import com.baydar.mapservice.util.Util;

@RestController
public class MapServiceController {
	@Autowired
	private Environment environment;

	@GetMapping("/map-service/from/{lat1},{lon1}/to/{lat2},{lon2}")
	public DistanceValue retrieveDistance(@PathVariable Double lat1, @PathVariable Double lon1, @PathVariable Double lat2,
			@PathVariable Double lon2) {
		
		DistanceValue distanceValue = new DistanceValue(lat1,lon1,lat2,lon2);
		distanceValue.setPort(Integer.parseInt(environment.getProperty("local.server.port")));
		distanceValue.setDistance(Util.calculateDistance(lat1, lon1, lat2, lon2));
		
		return distanceValue;
	}

}
