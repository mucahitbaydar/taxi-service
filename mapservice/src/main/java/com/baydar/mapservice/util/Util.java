package com.baydar.mapservice.util;

import uk.me.jstott.jcoord.LatLng;

public final class Util {
	
	public static double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
		LatLng lld1 = new LatLng(lat1, lon1);
        LatLng lld2 = new LatLng(lat2, lon2);
        Double distance = lld1.distance(lld2);
        return distance;
	}

}
